"use strict" ;

// Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.

// AJAX - це технологія, яка дозволяє веб-сторінці взаємодіяти з сервером без перезавантаження сторінки
// завдяки AJAX можна оновлювати окремі частини веб-сторінки без перезавантаження всієї сторінки, можна виконувати асинхронні операції

// #1 Отримати список фільмів серії Зоряні війни та вивести на екран список персонажів для кожного з них.

// Технічні вимоги:
// Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films та отримати список усіх фільмів серії Зоряні війни
// Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі. 
// Список персонажів можна отримати з властивості characters.
// Як тільки з сервера буде отримана інформація про фільми, 
// відразу вивести список усіх фільмів на екрані. Необхідно вказати номер епізоду, назву фільму, а також короткий зміст (поля episodeId, name, openingCrawl).
// Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран під назвою фільму.

async function fetchFilms() {
    const response = await fetch("https://ajax.test-danit.com/api/swapi/films");
    const films = await response.json();
    
    displayFilms(films);
    
    films.forEach((film) => {
      fetchCharacters(film.characters, film.episodeId, film.name);
    });
  }
  
  function displayFilms(films) {
    const filmsElement = document.getElementById("films");
    
    films.forEach((film) => {
      const filmElement = document.createElement("div");
      filmElement.classList.add("film");
      
      filmElement.innerHTML = `
        <h2>Episode ${film.episodeId} - ${film.name}</h2>
        <p>${film.openingCrawl}</p>
      `;
      
      filmsElement.appendChild(filmElement);
    });
  }
  
  async function fetchCharacters(charactersUrls, episodeId, filmName) {
    const characters = await Promise.all(
      charactersUrls.map((charactersUrl) =>
        fetch(charactersUrl).then((response) => response.json())
      )
    );
    
    displayCharacters(characters, episodeId, filmName);
  }
  
  function displayCharacters(characters, episodeId, filmName) {
    const charactersList = document.createElement("ul");
    charactersList.classList.add("characters-list");
    
    characters.forEach((character) => {
      const characterElement = document.createElement("li");
      characterElement.textContent = character.name;
      charactersList.appendChild(characterElement);
    });
    
    const filmElement = document.querySelector(`.film:nth-child(${episodeId})`);
    if (filmElement) {
      filmElement.appendChild(charactersList);
    }
  }
  
  fetchFilms();